import {Injectable} from '@angular/core';

import {Post} from './post.model';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {Subject, throwError} from 'rxjs';

@Injectable({providedIn: 'root'})
export class PostService {
  error = new Subject<string>();

  constructor(private http: HttpClient) {

  }

  createAndStorePost(title: string, content: string) {
    const postData: Post = {title, content};
    this.http
      .post(
        'https://angular-test-f7dfe.firebaseio.com/data/post.json',
        postData,
        {
          headers: new HttpHeaders({'Custom-Header': 'Hello'}),
          params: new HttpParams().set('print', 'pretty')
        }
      )
      .subscribe(responseData => {
        console.log(responseData);
      }, (error1: HttpErrorResponse) => {
        this.error.next(error1.message);
      });
  }

  onFetchPosts() {
    // Send Http request
    return this.http.get<{ [key: string]: Post }>('https://angular-test-f7dfe.firebaseio.com/data/post.json')
      .pipe(map((response: { [key: string]: Post }) => {
        const postsArray: Post[] = [];
        for (const key in response) {
          if (response.hasOwnProperty(key)) {
            postsArray.push({...response[key], id: key});
          }
        }
        return postsArray;
      }), catchError(err => {
        return throwError(err);
      }));
  }


}
