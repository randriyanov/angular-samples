import {Component, OnDestroy, OnInit} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpEventType} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Post} from './post.model';
import {PostService} from './post.service';
import {Subscription} from "rxjs";
import {tap} from "rxjs/internal/operators/tap";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  loadedPosts: Post[] = [];
  isFetching = false;
  error = null;
  private errorSubscription: Subscription;

  constructor(private http: HttpClient,
              private postService: PostService) {
  }

  ngOnInit() {
    this.onFetchPosts();
    this.errorSubscription = this.postService.error.subscribe(errorMessage => {
      this.error = errorMessage;
    });
  }

  onCreatePost(postData: Post) {
    // Send Http request
    this.postService.createAndStorePost(postData.title, postData.content);
  }

  onFetchPosts() {
    // Send Http request
    this.isFetching = true;
    this.postService.onFetchPosts()
      .subscribe(result => {
        this.isFetching = false;
        this.loadedPosts = result;
        console.log(result);
      }, (error1: HttpErrorResponse) => {
        this.error = error1.message;
      });
  }

  deletePosts() {
    this.http.delete('https://angular-test-f7dfe.firebaseio.com/data/post.json')
      .pipe(
        tap((event: HttpEventType) => {
          console.log(event);
        })
      )
      .subscribe(() => {
        this.loadedPosts = [];
      }, (error1: HttpErrorResponse) => {
        this.error = error1.message;
      });
  }


  onClearPosts() {
    // Send Http request
  }

  ngOnDestroy(): void {
    this.errorSubscription.unsubscribe();
  }
}
