import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {UserModel} from '../../../model/user.model';
import {HttpClient, HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  isFormSaved;
  savedUser;

  constructor(private httpClient: HttpClient) {
  }

  ngOnInit() {
  }

  userFormSubmit(userForm: NgForm) {
    const name = userForm.value.name;
    const login = userForm.value.login;
    const password = userForm.value.password;
    const user = new UserModel(login, password, name);
    this.httpClient.post('http://localhost:8080/angular-servlet', user, {observe: 'response'})
      .subscribe(
        (next: HttpResponse<any>) => {
          if (next.ok) {
            this.isFormSaved = true;
            this.savedUser = next.body;
          }
          console.log(next);
        }
      );
  }
}
