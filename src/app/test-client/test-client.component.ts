import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpResponse} from "@angular/common/http";
import {NgForm} from "@angular/forms";
import {UserModel} from "../model/user.model";

@Component({
  selector: 'app-test-client',
  templateUrl: './test-client.component.html',
  styleUrls: ['./test-client.component.css']
})
export class TestClientComponent {

  searchUser: UserModel;

  constructor(private httpClient: HttpClient) {

  }

  formPost(data: UserModel) {
    console.log(data.password, data.login);
    const option = new HttpParams().set('usr', data.login).set('pwd', data.password);
    this.httpClient.post('http://localhost:8080/angular-servlet',
      data,
    ).subscribe(response => {
        console.log(response);
      }, (error1: HttpErrorResponse) => {
        console.log(error1);
      }
    );
  }

  getUserByLogin(login: {search_login: string}) {
    console.log(login);
    this.httpClient.get('http://localhost:8080/angular-servlet',
      {
        params: new HttpParams().set('login', login.search_login)
      }
    ).subscribe(
      (response: UserModel) => {
        this.searchUser = response;
      }
    );
  }


}
