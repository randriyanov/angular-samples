export class UserModel {
  public id?: number;
  public login: string;
  public password: string;
  public name?: string;


  constructor(login: string, password: string, name?: string, id?: number)  {
    this.id = id;
    this.login = login;
    this.password = password;
    this.name = name;
  }
}
