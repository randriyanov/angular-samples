import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams, HttpUrlEncodingCodec} from '@angular/common/http';

@Component({
  selector: 'app-send-params',
  templateUrl: './send-params.component.html',
  styleUrls: ['./send-params.component.css']
})
export class SendParamsComponent implements OnInit {
  loadedPosts = [];

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
  }

  onCreatePost(postData: { title: string; content: string }) {
    // Send Http request
    let header = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    const body = new HttpParams()
      .set('login', postData.title)
      .set('password', postData.content);
    this.http
      .post(
        'http://localhost:8080/angular-servlet',
        postData,
        {headers: header},
      )
      .subscribe(responseData => {
          console.log(responseData);
        },
        error => {
          console.log(error);
        }
      )
    ;
  }

  onCreatePostDiffBody(postData: { title: string; content: string }) {
    // Send Http request
    const body = new HttpParams()
      .set('login', postData.title)
      .set('password', postData.content);
    this.http
      .post(
        'http://localhost:8080/angular-test',
        postData,
        {
          observe: 'events'
        }
      )
      .subscribe(responseData => {
          console.log(responseData);
        },
        error => {
          console.log(error);
        }
      )
    ;
  }

  onCreatePostFormUrlencoded(postData: { title: string; content: string }) {
    // Send Http request
    const codec = new HttpUrlEncodingCodec();
    let header = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });
    const body = new HttpParams()
      .set('login', postData.title)
      .set('password', postData.content);

    this.http
      .post(
        'http://localhost:8080/angular-servlet-url',
        body,
        {headers: header, responseType: 'text'}
      )
      .subscribe(responseData => {
          console.log(codec.decodeKey(responseData));
        },
        error => {
          console.log(error);
        }
      )
    ;
  }

  onFetchPosts() {
    // Send Http request
  }

  onClearPosts() {
    // Send Http request
  }

}
