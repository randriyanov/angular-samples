import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {SendParamsComponent} from './send-params/send-params.component';
import {TestClientComponent} from './test-client/test-client.component';
import {EmployeeComponent} from './form/employee/employee.component';
import {UserComponent} from './form/user/user.component';
import {RouterModule, Routes} from '@angular/router';
import {ShowComponent} from './form/user/show/show.component';
import {CreateUserComponent} from './form/user/create-user/create-user.component';


const appRoutes: Routes = [
  {
    path: 'user', component: UserComponent, children:
      [
        {path: 'create', component: CreateUserComponent}
      ]

  },
  {path: 'employee', component: EmployeeComponent}
];

@NgModule({
  declarations: [AppComponent, SendParamsComponent, TestClientComponent, EmployeeComponent, UserComponent, ShowComponent, CreateUserComponent],
  imports: [BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
